
Debian
====================
This directory contains files used to package madbyted/madbyte-qt
for Debian-based Linux systems. If you compile madbyted/madbyte-qt yourself, there are some useful files here.

## madbyte: URI support ##


madbyte-qt.desktop  (Gnome / Open Desktop)
To install:

	sudo desktop-file-install madbyte-qt.desktop
	sudo update-desktop-database

If you build yourself, you will either need to modify the paths in
the .desktop file or copy or symlink your madbyte-qt binary to `/usr/bin`
and the `../../share/pixmaps/bitcoin128.png` to `/usr/share/pixmaps`

madbyte-qt.protocol (KDE)

